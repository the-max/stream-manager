
let fs = require('fs');

window.$ = window.jQuery = require('jquery');
const x2js = require('x2js');

const { registerMiddleWare, startMiddleware } = require('./modules/middleware');
const { metar } = require('./modules/aviationinfo');
const { ejs } = require('./modules/htmlRenderer');

let config = require('../config');
$(() => {
    $.fn.delayKeyup = function (callback, ms) {
        var timer = 0;
        var el = $(this);
        $(this).keyup(function () {
            clearTimeout(timer);
            timer = setTimeout(function () {
                callback(el)
            }, ms);
        });
        return $(this);
    };

    let bs = require('bootstrap');
    $('.data-manager-field').delayKeyup(saveData, 500);

})

function saveData() {
    let $form = $('#data-form');
    let formData = $form.serializeArray();

    formData = formData.toObject();

    registerMiddleWare((renderData, next) => {
        console.log(renderData);
        let airports = [];
        if (renderData.from.length === 4) {
            airports.push(renderData.from);
        }
        if (renderData.to.length === 4) {
            airports.push(renderData.to);
        }
        metar.fetch(() => {

        });
    })

    registerMiddleWare((renderData, next) => {
        console.log(renderData);
        console.log('ENDE');
        //next();
    })
    registerMiddleWare((renderData, next) => {
        ejs.renderFile('template/layout.ejs', renderData, (err, data) => {
            if (err) throw err;
            console.log(data);
        });
    })

    console.log(formData);

    startMiddleware(formData);
}

/**
 * Turns an Array of key-values in "jQuery.serializeArray()" style into clean object key-value-pairs
 * 
 * @returns {Object} The Object
 */
Array.prototype.toObject = function () {
    let obj = {};
    for (let i = 0; i < this.length; i++) {
        let curElem = this[i];
        obj[curElem.name] = curElem.value;
    }
    return obj;
}

function fetchMetar() {

}