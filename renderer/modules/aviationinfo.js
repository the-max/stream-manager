let airports = [];

function fetchMetar() {
    if (airports.length > 0) {
        $.ajax({
            url: 'https://www.aviationweather.gov/adds/dataserver_current/httpparam?dataSource=metars&requestType=retrieve&format=xml&hoursBeforeNow=1&stationString=' + airports.join(','),
            method: 'GET',
            success: function (data) {
                let xmlParser = new x2js();
                let obj = xmlParser.dom2js(data);
                console.log(obj.response.data.METAR);
                renderData.metars = obj;
                next();
            }
        });
    }
}

module.exports = {
    metar: {
        fetch: fetchMetar
    }
};