const ejs = require('ejs');
let renderedData = {};

/**
 * 
 * @param {Object} renderData 
 * @param {boolean} useCache 
 */
function renderEjs(renderData, useCache = true) {
    let dataRenderable = renderData;
    if (useCache) {
        Object.assign(renderedData, renderData);
        dataRenderable = renderedData;
    }
    ejs.renderFile('template/layout.ejs', dataRenderable, (err, data) => {
        if (err) throw err;
        console.log(data);
    });
}

module.exports = {
    ejs: {
        render: renderEjs
    }
};