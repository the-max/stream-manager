let arr = [];
let i = 0;

/**
 * 
 * @param {function} fn 
 */
function registerMiddleWare(fn) {
    arr.push(fn);
}

/**
 * 
 * @param {Object} renderData 
 */
function startMiddleWare(renderData) {

    /**
     * Calls next middleware stage if available
     */
    function next() {
        i++;
        if (i < arr.length) {
            arr[i] && arr[i](renderData, next);
        }
    }
    arr[i] && arr[i](renderData, next);
}

module.exports = {
    registerMiddleWare: registerMiddleWare,
    startMiddleware: startMiddleWare
};