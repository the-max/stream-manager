module.exports = {
    out: {
        basePath: "out/",
    },
    files: {
        from: {
            title: "",
            path: "from.txt",
        },
        to: {
            title: "",
            path: "to.txt"
        },
        waypoints: {
            title: "",
            path: "waypoints.html",
            stringModifyer: function (value) {
                let wpArr = value.split(' ');
                return ' ' + wpArr.join(' - ') + ' |✈';
            }
        }
    }
};