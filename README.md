# Stream-Manager

Hey!  
Glad to see you up here.

This documentation seems quite empty yet, but it **WILL** get filled with a hopefully nice video and some information for you.

```
                                    |
BOEING 777                          |
by Jon Hyatt                        |
whatfer@u.washington.edu          .-'-.
                                 ' ___ '
                       ---------'  .-.  '---------
       _________________________'  '-'  '_________________________
        ''''''-|---|--/    \==][^',_m_,'^][==/    \--|---|-''''''
                      \    /  ||/   H   \||  \    /
                       '--'   OO   O|O   OO   '--'
```

----

Copyright 2018, distributed under [the Attribution-NonCommercial 4.0 International](https://gitlab.com/the-max/stream-manager/blob/master/LICENSE.md)-License.